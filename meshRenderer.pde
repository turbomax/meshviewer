////////////////////////////////////////////////////

// MESH RENDERER

///////////////////////////////////////////////////

// Imports
import peasy.*;
import toxi.geom.*;
import toxi.geom.mesh.*;
import toxi.processing.*;

// prefix for file (including absolute path)
String prefix = "";
// suffix for file (eg file extension and stuff)
String suffix = "";

int fileCount = 50;
int startOffset = 0;

// Graphics
ToxiclibsSupport gfx;

// Mesh stuff
String[] paths;
String[] names;
TriangleMesh[] meshes;
TriangleMesh now;

int frameSwitchCount = 2;
float meshRadius = 50;

// Camera Stuff
PeasyCam cam;
float rotation = 0.0f;
float rotdelta = 0.003f;

// Scrolling through models
long nextModelTime = 0;
int modelDeltaTime = 300;
int counter = 0;

// UI
int yDelta = 100;
color fgColor = color(255);
color bgColor = color(255, 100);


void setup() {


  //fullScreen(P3D);
  size(1920, 1080, P3D);
  background(0);
  //selectFolder("Select a folder to process:", "folderSelected");
  
  meshes = loadMeshSequence(prefix, suffix, fileCount, startOffset);

  cam = new PeasyCam(this, 100);
  cam.setMinimumDistance(0);
  cam.setMaximumDistance(400);
  cam.setActive(false);

  gfx=new ToxiclibsSupport(this);
}


void draw() {
  
  background(0);
  translate(0, 0, -100);
  pushMatrix();
  rotateX(PI);
  rotateY(PI/2 + rotation);
  rotation += rotdelta;

  noStroke();

  //fill(0);
  noFill();
  smooth();
  stroke(255);
  strokeWeight(0.5);

  if (meshes != null) {
    


    if(frameCount % frameSwitchCount == 0){
      counter++;
      counter%=meshes.length;
      now = meshes[counter];
    }
    
    if(now == null){
      now = meshes[counter];
    }
    
    //now.center(new Vec3D(0, 0, 0));
    gfx.mesh(now, false);

    cam.beginHUD();
    drawUI(50, 50, 100, 10, now, names[counter]);
    cam.endHUD();
  }

  popMatrix();
  
  saveFrame(frameCount + ".jpg");
}

void drawUI(int x, int y, int w, int h, TriangleMesh currentMesh, String name) {

  int textOffset = h * 5;
  textSize(24);
  pushMatrix();
  noStroke();
  fill(bgColor);
  translate(x, y);
  int uiCounter = 0;

  float data;

  fill(bgColor);
  //rect(0, uiCounter * yDelta, w, h);
  data = currentMesh.getNumFaces();
  //fill(fgColor);
  rect(0, uiCounter * yDelta, map(data, 0, 40000, 0, w), h);
  text(data, 0, uiCounter * yDelta + textOffset);
  uiCounter++;

  //rect(0, uiCounter * yDelta, w, h);
  data = currentMesh.getNumVertices();
  fill(fgColor);
  //rect(0, uiCounter * yDelta, map(data, 0, 40000, 0, w), h);
  text(data, 0, uiCounter * yDelta + textOffset);
  uiCounter++;

  //rect(0, uiCounter * yDelta, w, h);
  data = currentMesh.getBoundingBox().x;
  fill(fgColor);
  //rect(0, uiCounter * yDelta, map(data, 0, 40000, 0, w), h);
  text(data, 0, uiCounter * yDelta + textOffset);
  uiCounter++;

  
  //rect(0, uiCounter * yDelta, map(data, 0, 40000, 0, w), h);
  
  text(random(5000), 0, uiCounter * yDelta + textOffset);
  uiCounter++;
  
  text(random(200), 0, uiCounter * yDelta + textOffset);
  uiCounter++;
  
  text(random(100000), 0, uiCounter * yDelta + textOffset);
  uiCounter++;

  popMatrix();
  
  pushMatrix();
  translate(x,height-y);
  textSize(30);
  text(counter, 0, 0);  
  popMatrix();
}

void folderSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    paths = listFileNames(selection.getAbsolutePath());
    printArray(paths);
    meshes = loadMeshes(paths);
    loop();
  }
}

String[] listFileNames(String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    File[] files = file.listFiles();
    String[] paths = new String[files.length];

    for (int i = 0; i < paths.length; i++) {
      paths[i] = files[i].getAbsolutePath();
    }
    return paths;
  } else {
    return null;
  }
}

TriangleMesh[] loadMeshes(String[] paths) {
  TriangleMesh[] meshArray = new TriangleMesh[paths.length];
  names = new String[paths.length];

  for (int i = 0; i < paths.length; i++) {
    TriangleMesh currentMesh = (TriangleMesh)new STLReader().loadBinary(paths[i], STLReader.TRIANGLEMESH);
    float diam = currentMesh.getBoundingSphere().radius;
    meshArray[i]=currentMesh.getScaled(30).getRotatedX(-PI/2).getTranslated(new Vec3D(0,-45,0));

    String[] splitName = paths[i].split("/");
    names[i] = splitName[splitName.length-1];
  }
  return meshArray;
}


TriangleMesh[] loadMeshSequence(String _prefix, String _suffix, int _count, int _offset){
  
  
  TriangleMesh[] inMeshes = new TriangleMesh[_count];
  names = new String[_count];
  for(int i = 0; i < _count ; i++){
     
    String filename = String.format("%05d",i+_offset); 
    String path = _prefix + filename  + _suffix;
    println("loading -> " + path);
    TriangleMesh currentMesh = (TriangleMesh)new STLReader().loadBinary(path, STLReader.TRIANGLEMESH);
    
    inMeshes[i] = currentMesh.getScaled(30).getRotatedX(-PI/2).getTranslated(new Vec3D(0,-45,0));
    String[] splitName = path.split("/");
    names[i] = splitName[splitName.length-1];
  }
  return inMeshes;
}