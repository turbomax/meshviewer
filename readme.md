# Mesh renderer

used for displaying mesh sequences in wireframe view and a slight rotation

## how to use

```java

// prefix for file (including absolute path)
String prefix = "absolute/path/to/your/file/index";
// suffix for file (eg file extension and stuff)
String suffix = ".yourfileextension";

// use these to adjust the range of images
int fileCount = 50;
int startOffset = 0;

```

it will then render out all single frames as jpgs 🙃
